package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
)

func main() {

	log.WithFields(log.Fields{
		"animal": "walrus",
	}).Info("A walrus appears")

	logrus.Info("sdfd")
	r := gin.Default()

	r.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"health": true,
		})
	})

	if err := r.Run(":8090"); err != nil {
		logrus.WithError(err).Fatal("Couldn't listen")
	}
}
